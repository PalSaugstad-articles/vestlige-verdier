.. raw:: html

   <!-- (php # 2 __DATE__ Vestlige verdier) -->
   <!-- ($attrib_AC=0;) -->

================
Vestlige verdier
================

*Av: |author| / |datetime|*

.. include:: revision-header.rst

Jeg lurer på, jeg, hva *vestlige verdier* er.
Noen politikere bruker begrepet oftere enn andre.
Erna Solberg har brukt det ved flere anledninger.
Det høres jo flott ut da, tenk, *verdier*, og så attpå til *vestlige*!
Hvis en sjekker norske wikipedia, så blir en ikke klokere, der finnes det ingen artikkel om *vestlige verdier*.
I den engelske utgaven finnes *Western thought*, og denne reflekterer omtrent samme frustrasjon over begrepet som jeg har selv.

Det hele er liksom så ullent, ingen sier konkret hva de mener med *vestlige verdier*,
annet enn kanskje ytringsfrihet og menneskerettigheter og slikt.
Men det må jo omfatte noe mer!
Lover og regler for hvordan hele samfunnet fungerer må da vel kunne trekkes inn i verdi-grunnlaget vårt?

For eksempel reglene for hvordan bank- og finaniserings-systemene fungerer.
I noen samfunn og kulturer er jo renter og gjeld ikke stuerent. Men i den vestlige verden er det *helt* OK, virker det som.
Det finnes knapt noen mekanisme i vår kultur som er mer infiltrert uansett hvor du snur deg enn gjeld og renter.
For det å tjene penger på andres mer eller mindre veloverveide behov for å skaffe seg 'stuff' er helt OK.
Gjeld fra enkelt-personer kan jo til og med pakkes inn i nye "finansielle instrumenter"
og selges videre til andre investorer helt til man har mistet fullstendig oversikten.
Slike transaksjoner er visstnok veldig bra, for da kan jo *markedet* avgjøre *riktig pris*,
og *det* er veldig viktig, og en *vestlig verdi* som vi må omfavne alle sammen, tydeligvis.

Og hva med det å ta vare på hverandre, det kommer vel et stykke ned på listen over *vestlige verdier*.
Kommer ikke egoisme høyere opp på listen tro?
Ayn Rand og andre som setter individualisme og egoisme som et ideal,
er ofte forbilder for politikere som liker å trekke fram *vestlige verdier*:
Det er jo gjennom egoisme at vi kommer oss opp og fram her i verden.

Det gjelder å være best.

Det beste landet, som Tyskland, som har en stor og effektiv industri som ekporterer for store verdier hvert år.
Eller som Norge, som har stor formue i utlandet i form av noe vi kaller *Pensjonsfond, utland*.
Og hvis vi forvalter denne formuen riktig, sier økonomene med alvorlige ansikter, så kan vi høste av rentene fra fondet i all evighet, dere!
Alle ser opp til land som lykkes slik som Norge og Tyskland. Alle vil bli som dem.

Det er makroversjonen av *Den Amerikanske Drømmen*. Enhver har mulighet til å bli best! Det er det som gjelder!
*Det* er den *vestlige verdien* sin, det!

Vi trenger de rike, for de kan med sin rikdom sette igang nye hjul, så de rike må få skattelette, altså.
Det er ikke så farlig at vi ikke helt ser hvilke hjul de skal sette igang nå.
Bare de får litt mer penger å rutte med,
så finner de nok på noe, helt sikkert, altså, for de vil jo helst bli enda rikere, ikke sant?

Men så kommer noen å tukler med verdiene.
De sier f. eks. at atomvåpen ikke er så bra, at vi bør satse på å destruere dem alle sammen, og rett og slett klare oss uten.
Akkurat som at vi ikke må være best på atomvåpen også, eller hva?

Og så Paris-avtalen da dere. En stor, stor selvmotsigelse.
Akkurat som at vi skal bli best på å forårsake at en hel industri går i oppløsning?
Hva blir det neste, Mat-kaste-lov kanskje?
To tanker i hodet på én gang er visst veldig bra, og helt nødvendig.
Jeg tror det må være en veldig sentral *vestlig verdi*, det å ha to tanker i hodet på én gang.
Vi må ha fullt kjør på nye energi-løsninger i tillegg til olje og gass,
men det betyr jo ikke at vi skal *slutte* med olje og gass! Hørt sånt tullprat, da gitt.

*Atomvåpenforbud* og *Parisavtalen* er jo nokså konkrete prosjekter, og det skaper diskusjoner og forventninger, samhørighet og motsetninger.

*Vestlige verdier* er ikke fullt så konkret.
Det skal visst skape samhørighet og forventninger, har jeg forstått.
Vi er visst enige om at det er veldig, veldig bra.

Men hva *er* det?

.. raw:: html

   <hr>
   <a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Vestlige%20verdier">Tweet</a>
   <br>
   <a target="_blank" href="show?f=articles/parsed/vestlige-verdier/nor/vestlige-verdier.pdf">som pdf</a>
